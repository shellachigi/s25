/***
 * 2. Use the count operator to count the total number of fruits on sale.
 * Answer:
 * db.fruits.aggregate( [ 
    { $match: {onSale: true} },
    { $count: "total_onsale" }
    ] )
3. Use the count operator to count the total number of fruits with stock more than 20.
Answer:
db.fruits.aggregate( [ 
    { $match: {stock: {$gte:20} } },
    { $count: "stocks_high_20" }
    ] )
4. Use the average operator to get the average price of fruits onSale per supplier.
Answer:
db.fruits.aggregate( [ 
    { $match: {onSale: true } },
    { $group: {_id: "$supplier_id", avgPrice: { $avg: "$price" } } }
    ] )

5. Use the max operator to get the highest price of a fruit per supplier.
Answer:
db.fruits.aggregate( [ 
    { $match: {onSale: true } },
    { $group: {_id: "$supplier_id", maxPrice: { $max: "$price" } } }
    ] )

6. Use the min operator to get the lowest price of a fruit per supplier.
Answer:
db.fruits.aggregate( [ 
    { $match: {onSale: true } },
    { $group: {_id: "$supplier_id", minPrice: { $min: "$price" } } }
    ] )


 * 
 */



